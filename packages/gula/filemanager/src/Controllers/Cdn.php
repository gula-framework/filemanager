<?php

namespace Gula\Filemanager\Controllers;

use Gula\Filemanager\Models\ImageResizer;

class Cdn extends \App\Http\Controllers\Controller
{
    protected $cachePath = '';

    public function __construct()
    {
        $this->cachePath = storage_path() . '/cache/';
    }

    public function Index($path)
    {
        $cacheUrl = $this->cachePath . $this->getHashUrl($path);

        if (file_exists($cacheUrl)) {
            $im = file_get_contents($cacheUrl);
            header('Cache-Control:public,max-age=31536000');
            header('content-type: ' . mime_content_type($cacheUrl));
            echo $im;
            exit();
        }

        $image = new ImageResizer();
        $image->setContent($path, true, $cacheUrl);

        $content = $image->getContent();
        $status = $image->getStatus();
        $httpHeader = $image->getHttpHeaders();

        if ($content) {
            header($status);

            foreach ($httpHeader as $httpHeaderItem) {
                header($httpHeaderItem);
            }

            echo $content;
            exit();
        }
    }

    protected function getHashUrl($path)
    {
        $sUrl = explode('/', $path);
        $fileName = end($sUrl);

        return md5($path) . '_' . $fileName;
    }
}
