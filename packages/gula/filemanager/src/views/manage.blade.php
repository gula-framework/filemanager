@extends('filemanager::base')
@section('less')

@endsection
@section('content')
    <style>
        .folderstructure {
            /*background-color: #e2e8f0;*/
            padding-right: 10px;
        }

        .folderstructure a {
            color: black;
            text-decoration: none !important;
        }

        .folderSelection {
            text-align: center;
            font-size: 12px;
            cursor: pointer;
        }
    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{asset('dropzone/dist/dropzone.css')}}">
    <script src="{{asset('dropzone/dist/min/dropzone.min.js')}}" type="text/javascript"></script>

    <div class="containerList">
        <div class="row">
            <div class="col-md-12">
                <br/>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="https://cms.gula.nl/resizer/36x36/cms/icons/image_manager.png"/>&nbsp;<b>Afbeeldingen
                                    beheren</b>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class='content'>
                            <div class="row">
                                <div class="col-md-2 folderstructure">
                                    <h3>Mappen</h3>
                                    {!!  $structure['structureHtml']!!}
                                </div>

                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">{!! $structure['breadCrumbs'] !!}</div>
                                        </div>
                                    </div>
                                    <div class="row ßfolderSelection">
                                        <div class="col-2">

                                            <img src="{!! '/resizer/70x70/icons/open_folder.png' !!}"/>
                                            <b style="text-decoration: 10px">{{$structure['currentFolder']}}</b>
                                        </div>
                                    </div>
                                    <div class="row folderSelection">
                                        @foreach($structure['subfolders'] as $subfolder)
                                            <div class="col-1">
                                                <a href="{{$subfolder['path']}}" style="text-decoration: none">
                                                    <img src="{!! '/resizer/70x70/icons/folder.png' !!}"/>
                                                    {{$subfolder['name']}}
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row files">
                                        @foreach($structure['files'] as $image)
                                            <div class="col-2">
                                                <img src="/resizer/180x180/{{$image}}" />
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone(".dropzone", {
            maxFilesize: 3,  // 3 mb
            acceptedFiles: ".jpeg,.jpg,.png,.pdf",
            init: function () {
                this.on('complete', function () {
                    location.reload();
                });
            }
        });
        myDropzone.on("sending", function (file, xhr, formData) {
            formData.append("_token", CSRF_TOKEN);
        });
    </script>
@endsection
