<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'web', 'namespace' => 'Gula\Filemanager\Controllers'], function()
{
    Route::get('/resizer/{path}', ['uses' => 'Cdn@index'])->where('path', '.+');

    Route::get('/upload', ['uses' => 'Upload@index']);
    Route::get('/manage', ['uses' => 'Manage@index']);
});

Route::get('filemanager', function() {
    echo 'Hello from the filemanager package!';
});


//filemanager

//resizer

//uploader
