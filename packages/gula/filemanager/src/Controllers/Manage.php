<?php

namespace Gula\Filemanager\Controllers;

use Illuminate\Http\Request;

class Manage extends \App\Http\Controllers\Controller
{

    public function index(Request $request)
    {
        $path = $request->get('path') ?? '/cdn';
//        if(substr($path, 0,5) === '/home'){
//            $path = '/cdn' . substr($path,5);
//        }

        $structure = getFolder('/cdn', $path);

        return view('filemanager::manage', compact('structure'));

print_r($dir);
    }

    public function addFolder(Request $request)
    {

    }

    public function deleteFolder(Request $request)
    {

    }

    public function renameFolder(Request $request)
    {

    }

    public function moveFile(Request $request)
    {

    }

    public function deleteFile(Request $request)
    {

    }

    public function renameFile(Request $request)
    {

    }

    public function updateFileInfo (Request $request)
    {

    }

    public function rotateFile(Request $request)
    {

    }
}
