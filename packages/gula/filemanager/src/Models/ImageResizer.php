<?php

namespace Gula\Filemanager\Models;
use Gregwar\Image\Image;

class ImageResizer extends \Illuminate\Database\Eloquent\Model
{
    private $location = '';
    private $imageNotFound = '';
//    private $cachePath = '../storage/cdn/cache/';
    private $imagePath;
    private $width;
    private $height;
    private $file;
    private $status;
    private $content;

    public function __construct(array $attributes = [])
    {
        $this->location = storage_path() . '/cdn';
        $this->imageNotFound = storage_path() . '/images/notfound.jpg';
    }

    public function getHttpHeaders()
    {
        if($this->status == 'HTTP/1.1 200')
        {
            $lastModified=filemtime($this->file);
            $etagFile = md5_file($this->file);
        } else {
            $lastModified=filemtime($this->imageNotFound);
            $etagFile = md5_file($this->imageNotFound);
        }

        return array('Cache-Control:public,max-age=31536000',"Content-Type: " . (file_exists($this->file) ? mime_content_type($this->file) : 'image/png'),"Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified),"Etag: $etagFile");
    }
    public function getContent()
    {
        return $this->content;
    }
    public function getStatus()
    {
        return $this->status;
    }
    public function setContent($path, $save = false, $cacheUrl = null)
    {
        $request = explode("/", $path);
        $size = explode("x",$request[0]);
        $this->width = $size[0];
        $this->height = $size[1];

        for ($x = 1; $x <= count($request)-2; $x++) {
            $this->imagePath.= "/" . $request[$x];
        }

        $this->file = $this->location . $this->imagePath. '/' . $request[count($request)-1];

        $this->status = 'HTTP/1.1 200';

        if($this->width <= 3000 && $this->height <= 2000)
        {
            try {
                $this->content = Image::open($this->file)
                    ->resize($this->width,$this->height)
                    ->get();

                if(!file_exists($this->file))
                {
                    $this->content = Image::open($this->imageNotFound)
                        ->resize($this->width,$this->height)
                        ->get();

                    $this->status = 'HTTP/1.1 404';
                } else {
                    Image::open($this->file)
                        ->resize($this->width,$this->height)
                        ->save($cacheUrl);
                }

            } catch (\Exception $e) {

            }
        }
    }

}
