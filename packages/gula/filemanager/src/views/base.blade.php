<!DOCTYPE html>
<html lang="nl">
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    @yield('meta')

    <link href='https://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    @yield('css')
    <link href="{{asset('less/website-cms.css')}}" rel="stylesheet">
    <link rel="icon" href="/images/favicon_gula.png" sizes="32x32"/>
    <title>Cms Gula</title>
    @yield('header')

</head>
<body class="body">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/cms"><img src="https://cms.gula.nl/resizer/130x30/cms/logo/gula_logo.png"
                                             style="border:none;margin-top: -6px;margin-left: 5px;">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false" title="Pagina's">
                    <img src="https://cms.gula.nl/resizer/36x36/cms/icons/layout.png" title="Pagina's"/>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <div class="dropdown-item disabled">Pagina's</div>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/cms/pages/edit/1"><img
                            src="https://cms.gula.nl/resizer/36x36/cms/icons/page.png" class="symbolRow"/>
                        Voorpagina</a>
                    <a class="dropdown-item" href="/cms/pages/edit/2"><img
                            src="https://cms.gula.nl/resizer/36x36/cms/icons/page.png" class="symbolRow"/>
                        Onze planken</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false" title="Bestellingen">
                    <img src="https://cms.gula.nl/resizer/36x36/cms/icons/shop.png" title="Bestellingen"/>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <div class="dropdown-item disabled">Webshop</div>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/cms/shop_orders/list"><img
                            src="https://cms.gula.nl/resizer/36x36/cms/icons/pos.png" class="symbolRow"/>
                        Bestellingen</a>
                    {{--                    <a class="dropdown-item" href="/cms/shop_opening_hours/list"><img--}}
                    {{--                            src="https://cms.gula.nl/resizer/36x36/cms/icons/calendar.png" class="symbolRow"/>--}}
                    {{--                        Bezorgdata</a>--}}
                    <div class="dropdown-divider"></div>
                    <div class="dropdown-item disabled">Content</div>
                    <div class="dropdown-divider"></div>

                    <a class="dropdown-item" href="/cms/shop_categories/list">
                        <img src="https://cms.gula.nl/resizer/36x36/cms/icons/categories.png"
                             class="symbolRow"/> Categorieën
                    </a>
                    <a class="dropdown-item" href="/cms/shop_products/list">
                        <img src="https://cms.gula.nl/resizer/36x36/cms/icons/products.png"
                             class="symbolRow"/> Producten
                    </a>
                    <a class="dropdown-item" href="/cms/shop_additional_products/list">
                        <img src="https://cms.gula.nl/resizer/36x36/cms/icons/titles.png"
                             class="symbolRow"/> Bijbestelbare bijproducten
                    </a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false" title="Mail Instellingen">
                    <img src="https://cms.gula.nl/resizer/36x36/cms/icons/mail.png" title="Mail Instellingen"/>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <div class="dropdown-item disabled">E-Mail</div>
                    <div class="dropdown-divider"></div>
                    <div class="dropdown-item disabled">Tekst sjablonen</div>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/cms/shop_templates/edit/order_notification"><img
                            src="https://cms.gula.nl/resizer/36x36/cms/icons/edit.png" class="symbolRow"/>
                        Bestelling binnen</a>
                    <a class="dropdown-item" href="/cms/shop_templates/edit/order_confirmation"><img
                            src="https://cms.gula.nl/resizer/36x36/cms/icons/edit.png" class="symbolRow"/>
                        Bevestiging bestelling voor klant</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/cms/image_manager" title="Afbeeldingen beheren">
                    <img src="https://cms.gula.nl/resizer/36x36/cms/icons/image_manager.png" class="symbolRow"/>
                </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="/cms" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false" title="Instellingen van het CMS">
                    <img src="https://cms.gula.nl/resizer/36x36/cms/icons/settings.png" title="Instellingen"/>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <div class="dropdown-item disabled">Instellingen</div>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/cms/settings/company_info"><img
                            src="https://cms.gula.nl/resizer/36x36/cms/icons/company_info.png" class="symbolRow"/>
                        Bedrijfsinformatie
                    </a>
                    <a class="dropdown-item" href="/cms/settings/psp"><img
                            src="https://cms.gula.nl/resizer/36x36/cms/icons/euro.png" class="symbolRow"/>
                        Betaalmodule (PSP)
                    </a>
                </div>
            </li>


        </ul>
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="">{{ __('Login') }}</a>
                </li>
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href=""
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>

        {{--        <form class="form-inline my-2 my-lg-0">--}}
        {{--            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">--}}
        {{--            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>--}}
        {{--        </form>--}}
    </div>
</nav>
@yield('crumblepath')
{{--@yield('selectContainer')--}}
@yield('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script src="{{asset('js/cms.js')}}" ></script>
@yield('js')
</body>
</html>

